// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusController.h"
#include "Food.h"
#include "SpeedBust.h"
#include "Poison.h"

// Sets default values
ABonusController::ABonusController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnCountBonus = 6;
	SpawnCycle = 15.f;
	StartLocation = FVector(-475, -475, -30);
	StepSpawn = 50.f;
	MaxCountX = 20;
	MaxCountY = 20;
}

// Called when the game starts or when spawned
void ABonusController::BeginPlay()
{
	Super::BeginPlay();	
	TimeRemainToSpawn = 0;
}

// Called every frame
void ABonusController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimeRemainToSpawn -= DeltaTime;

	if (TimeRemainToSpawn <= 0)
	{
		TimeRemainToSpawn = SpawnCycle;

		SpawnBonus();
	}
}

void ABonusController::SpawnBonus()
{
	for (int i = 0; i < SpawnCountBonus; i++)
	{
		float X = FMath::RandRange(0, MaxCountX-1) * StepSpawn;
		float Y = FMath::RandRange(0, MaxCountY-1) * StepSpawn;
		FVector Location(StartLocation.X + X, StartLocation.Y + Y, StartLocation.Z);
		FTransform NewTransform(Location);

		float Random = FMath::RandRange(0.f, 1.f);
		if (Random <= 0.2)
		{
			GetWorld()->SpawnActor<APoison>(PoisonClass, NewTransform);
		}
		else if (Random <= 0.4)
		{
			GetWorld()->SpawnActor<ASpeedBust>(SpeedBustClass, NewTransform);
		}
		else
		{
			GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
		}
	}
}

