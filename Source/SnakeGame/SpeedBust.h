// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Food.h"
#include "SpeedBust.generated.h"

UCLASS()
class SNAKEGAME_API ASpeedBust : public AFood
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeedBust();

	UPROPERTY(EditDefaultsOnly)
	float SpeedBoost;

	UPROPERTY(EditDefaultsOnly)
	float DurationBoost;
protected:
	// Called when the game starts or when spawned	
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool IsHead) override;
};
