// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBust.h"
#include "SnakeBase.h"

// Sets default values
ASpeedBust::ASpeedBust()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DurationBoost = 10.f;
	SpeedBoost = 2.f;
}

// Called when the game starts or when spawned
void ASpeedBust::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ASpeedBust::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpeedBust::Interact(AActor* Interactor, bool IsHead)
{
	if (IsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->SetCurrentSpeed(SpeedBoost, DurationBoost);
			this->Destroy();
		}
	}
}