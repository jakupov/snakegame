// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BonusController.generated.h"

class AFood;
class APoison;
class ASpeedBust;

UCLASS()
class SNAKEGAME_API ABonusController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusController();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APoison> PoisonClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpeedBust> SpeedBustClass;

	UPROPERTY(EditDefaultsOnly)
	int32 SpawnCountBonus;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxCountX;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxCountY;

	UPROPERTY(EditDefaultsOnly)
	float SpawnCycle;

	float TimeRemainToSpawn;

	FVector StartLocation;

	float StepSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnBonus();
};
