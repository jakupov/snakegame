// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MoveSpeed = 10.f;
	SpeedBoostDuration = -1.f;
	LastMoveDirection = EMovementDirection::RIGHT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetCurrentSpeed(1, -1);
	AddSnakeElemnt(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	
	if (SpeedBoostDuration >= 0)
	{
		SpeedBoostDuration -= DeltaTime;
		if (SpeedBoostDuration < 0)
		{
			SetCurrentSpeed(1, -1);
		}
	}
}

void ASnakeBase::AddSnakeElemnt(int ElementsCount)
{
	for (int i = 0; i < ElementsCount; i++)
	{
		FVector NewLocation(475, -325-SnakeElements.Num() * ElementSize, -30);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementBaseClass, NewTransform);
		NewSnakeElement->MeshComponent->SetVisibility(false);
		NewSnakeElement->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();			
			NewSnakeElement->MeshComponent->SetVisibility(true);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0, 0, 0);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		ASnakeElementBase* CurrentElement = SnakeElements[i];
		ASnakeElementBase* PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlapedElement, AActor* Other)
{
	if (IsValid(OverlapedElement))
	{
		int32 IndexElement;
		SnakeElements.Find(OverlapedElement, IndexElement);
		bool IsHead = IndexElement == 0;
		IInteractable* Interact = Cast<IInteractable>(Other);

		if (Interact)
		{
			Interact->Interact(this, IsHead);
		}
	}
}

void ASnakeBase::Death()
{
	if (IsValid(PawnOwner))
	{
		PawnOwner->CreateSnakeBase();
	}
}

void ASnakeBase::DestroyBody(int Count)
{
	int32 ArrayCount = SnakeElements.Num();
	Count = (Count > 0) ? Count : ArrayCount;
	for (int i = ArrayCount; i > ArrayCount - Count; i--)
	{
		ASnakeElementBase* Element = SnakeElements[i - 1];		
		SnakeElements.Remove(Element);
		Element->Destroy();
	}
}

void ASnakeBase::SetCurrentSpeed(float SpeedBoost, float Duration)
{
	SetActorTickInterval(MoveSpeed / SpeedBoost);
	SpeedBoostDuration = Duration;
}

