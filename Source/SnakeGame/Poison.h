// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Food.h"
#include "Poison.generated.h"

UCLASS()
class SNAKEGAME_API APoison : public AFood
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APoison();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool IsHead) override;
};
